-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2018 at 05:15 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `nasabah`
--

CREATE TABLE IF NOT EXISTS `nasabah` (
  `no_rek` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jumlah` decimal(14,2) NOT NULL,
  `jenis` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nasabah`
--

INSERT INTO `nasabah` (`no_rek`, `nama`, `jumlah`, `jenis`) VALUES
('0000000001', 'John Doe', '10000.00', 'tabungan'),
('0000000002', 'Veronica', '30000.00', 'tabungan'),
('0000000003', 'Friday', '30000.00', 'deposito'),
('0002988748', 'Jarvis', '40000.00', 'Deposito'),
('0098738894', 'Dimas Dwi B', '210000.00', 'tabungan'),
('009893099859', 'Dimas Dwi B', '340000.00', 'tabungan'),
('098475830209', 'John Connor', '1267000.00', 'deposito'),
('0988787765633', 'Test Nasabah', '20000000.00', 'tabungan'),
('908838465800', 'Dimas Dwi', '2600000.00', 'tabungan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nasabah`
--
ALTER TABLE `nasabah`
 ADD PRIMARY KEY (`no_rek`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
